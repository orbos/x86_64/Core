#!/usr/bin/env bash

temp_pass1()
{
  dynamic_linker_fix()
  {
    # for file in \
    # $(find gcc/config -name linux64.h -o -name linux.h -o -name sysv4.h)
    # do
    while IFS= read -r file
      do
      cp -uv $file{,.orig}
      sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
          -e 's@/usr@/tools@g' $file.orig > $file
      echo '
  #undef STANDARD_STARTFILE_PREFIX_1
  #undef STANDARD_STARTFILE_PREFIX_2
  #define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
  #define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
      touch $file.orig
  done < <(find gcc/config -name linux64.h -o -name linux.h -o -name sysv4.h)
  }
  x86_64fix()
  {
    case $(uname -m) in
    x86_64)
      sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
      ;;
    esac
  }
  dlf1()
  {
    dynamic_linker_fix
    x86_64fix
  }
  cmddesc+=([dlf1]="Performs a needed dynamic linker and stack protection fix.")
  extrafxn+=("dlf1")
  for function in "${extrafxn[@]}"
  do
    buildopts+=(["$function"]="$function")
  done
}
cleanupfunc+=("temp_pass1" "dynamic_linker_fix" "stack_protection_fix" "dlf1")

temp_pass2()
{
  dlf2()
  {
    cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
    $(dirname $($ORBOS_TGT-gcc -print-libgcc-file-name))/include-fixed/limits.h
    temp_pass1
    dlf1
  }
  cmddesc+=([dlf2]="Performs a needed dynamic linker fix.")
  extrafxn+=("dlf2")
  
  lngcc()
  {
    ln -sv gcc /tools/bin/cc
  }
  cmddesc+=(["lngcc"]="Create a needed symlink for this stage of compilation.")
  extrafxn+=("lngcc")
  
  ctc()
  {
    echo 'main(){}' > dummy.c
    "$ORBOS_TGT"-gcc dummy.c
    readelf -l a.out | grep ': /tools'
    rm -v dummy.c a.out
  }
  cmddesc+=(["ctc"]="Check the GCC toolchain to make sure it is functioning correctly.")
  extrafxn+=("ctc")
  
  for function in "${extrafxn[@]}"
  do
    buildopts+=(["$function"]="$function")
  done
}
cleanupfunc+=("temp_pass2" "dlf2" "lngcc" "ctc")

cleanup_extras()
{
  for function in "${cleanupfunc[@]}"
  do
    unset -f "$function"
  done
}
