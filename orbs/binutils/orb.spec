http://ftp.gnu.org/gnu/binutils/binutils-2.27.tar.bz2

http://www.gnu.org/software/binutils/
The GNU Binutils are a collection of binary tools.





temp_pass1
Temporary toolchain pass 1 compile of binutils.

cs e sbd c m is64 mi
c
../binutils-2.27/configure

--prefix=/tools --with-sysroot=$orbos --with-lib-path=/tools/lib --target=$ORBOS_TGT --disable-nls --disable-werror
y
temp_pass2
Temporary toolchain pass 2 compile of binutils.

cs e sbd c m mi m1 m2 cp
c cp
../binutils-2.27/configure
CC=$ORBOS_TGT-gcc AR=$ORBOS_TGT-ar RANLIB=$ORBOS_TGT-ranlib
--prefix=/tools --disable-nls --disable-werror --with-lib-path=/tools/lib --with-sysroot
cp

-v ld/ld-new /tools/bin

