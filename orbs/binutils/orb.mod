#!/usr/bin/env bash

temp_pass1()
{
  unset -v extrafxn
  is64()
  {
    case $(uname -m) in
      x86_64) mkdir -pv /tools/lib && ln -sv lib /tools/lib64 ;;
    esac
  }
  cmddesc+=([is64]="Creates toolchain /tools/lib directory and /tools/lib64 symlink.")
  extrafxn+=("is64")
  for function in "${extrafxn[@]}"
  do
    buildopts+=(["$function"]="$function")
  done
}
cleanupfunc+=("temp_pass1" "is64")
cleanupvar+=("extrafxn" "function")

temp_pass2()
{
  unset -v extrafxn
  m1()
  {
    makeopts=("-C" "ld" "clean")
    make "${makeopts[@]}"
  }
  cmddesc+=([m1]="Runs: make -C ld clean")
  m2()
  {
    makeopts=("-C" "ld" "LIB_PATH=/usr/lib:/lib")
    make "${makeopts[@]}"
  }
  cmddesc+=([m1]="Runs: make -C ld LIB_PATH=/usr/lib:/lib")
  extrafxn+=("m1" "m2")
  for function in "${extrafxn[@]}"
  do
    buildopts+=(["$function"]="$function")
  done
}
cleanupfunc+=("temp_pass2" "m1" "m2" "cleanup_extras")
cleanupvar+=("makeopts" "function" "var")

cleanup_extras()
{
  for function in "${cleanupfunc[@]}"
  do
    unset -f "$function"
  done
  for var in "${cleanupvar[@]}"
  do
    unset -v "$var"
  done
}