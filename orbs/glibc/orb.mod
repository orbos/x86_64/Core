#!/usr/bin/env bash

temp()
{
  ctc()
  {
    echo 'main(){}' > dummy.c
    "$ORBOS_TGT"-gcc dummy.c
    readelf -l a.out | grep ': /tools'
  }
  cmddesc+=([ctc]="Check the GLIBC toolchain to make sure it is functioning correctly.")
  extrafxn+=("ctc")
  
  for function in "${extrafxn[@]}"
  do
    buildopts+=(["$function"]="$function")
  done
}
cleanupfunc+=("temp" "ctc" "cleanup_extras")

cleanup_extras()
{
  for function in "${cleanupfunc[@]}"
  do
    unset -f "$function"
  done
}
