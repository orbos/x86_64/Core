#!/usr/bin/env bash

temp()
{
  newcert()
  {
    openssl x509 -in MyRootCA.pem -text -fingerprint -setalias "My Root CA 1"     \
        -addtrust serverAuth -addtrust emailProtection -addreject codeSigning \
        > MyRootCA-trusted.pem
  }
}