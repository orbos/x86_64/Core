http://ftp.gnu.org/gnu/gettext/gettext-0.19.8.1.tar.xz

http://www.gnu.org/software/gettext/
The GNU gettext utilities are a set of tools that provides a framework to help other GNU packages produce multi-lingual messages.





temp
Temporary toolchain compile of gettext.

cs e pd c m1 m2 m3 m4 m5 cp
pd c m1 m2 m3 m4 m5 cp
pd gettext-tools


./configure
EMACS=\"no\"
--prefix=/tools --disable-shared
make

-C gnulib-lib
make

-C intl pluralx.c
make

-C src msgfmt
make

-C src msgmerge
make

-C src xgettext
cp

-v src/{msgfmt,msgmerge,xgettext} /tools/bin
