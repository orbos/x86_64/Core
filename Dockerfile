FROM registry.gitlab.com/orbos/dev-tools

RUN mkdir -p /build /fakeroot && ln -sv /build/tools /

COPY . /buildfiles/Core

RUN chown -R orbos:orbos /var/log/orbos /buildfiles /build /fakeroot

RUN rm -rf /buildfiles/Core/{Dockerfiles,Dockerfile,.git*,README.md}

VOLUME ["/orb", "/build"]

USER 1000
